defmodule Hello.User do
  use Hello.Web, :model

  schema "users" do
    field :name, :string
    field :email, :string
    field :profile, :string
    field :password, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :email, :profile, :password])
    |> validate_required([:name, :email, :profile, :password])
  end
end
